﻿namespace Coscine.Api.LegacySharePoint.Utils
{
    class PowerShellUtil
    {
        public static string GetTemplatesScript(string tempDirectory)
        {
            return @"$extractPath =  'C:\Programs\Consul\'
                $fileName = 'Consul.exe'
                $fullPath = ($extractPath + $fileName)

                $tempDirectory = '{{tempDirectory}}'

                $gitlab_token = & $fullPath kv get 'coscine/global/gitlabtoken'

                git clone https://gitlab-ci-token:$gitlab_token@git.rwth-aachen.de/coscine/backend/sharepoint/templates.git $tempDirectory

                cd $tempDirectory

                $pagebranch = & $fullPath kv get 'coscine/local/sharepoint/pagebranch'

                if (!$pagebranch) {
	                $pagebranch = 'master';
                }

                git checkout $pagebranch".Replace("{{tempDirectory}}", tempDirectory);
        }
    }
}

﻿namespace Coscine.Api.LegacySharePoint.Models
{
    public class SharePointEventObject
    {
        public string SharePointSite { get; set; }
        public string ProjectDescription { get; set; }
        public string ProjectDisplayName { get; set; }
        public string ProjectSlug { get; set; }
        public string StsPrefix { get; set; }
        public string UserId { get; set; }
        public string Role { get; set; }

        public SharePointEventObject()
        {

        }
    }
}

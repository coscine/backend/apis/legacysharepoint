﻿using Coscine.Api.LegacySharePoint.Models;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using System;

namespace Coscine.Api.LegacySharePoint.Utils
{
    public class SharePointUtil
    {
        public static string EnsureProjectUrl(SPSite site, SharePointEventObject sharePointEventObject)
        {
            //XXX: remove dependency to SPSite directly use siteUrl as parameter

            var uri = new Uri(site.Url);
            var authority = uri.Authority;
            var rootUri = sharePointEventObject.SharePointSite.Substring(0, sharePointEventObject.SharePointSite.IndexOf(authority) + authority.Length);
            //XXX: make this a configurable
            var projectPath = "p";

            //XXX: ensure this in the install script
            SPPrefixCollection prefixColl = site.WebApplication.Prefixes;
            if (prefixColl.Contains(projectPath) == false)
            {
                site.WebApplication.Prefixes.Add(projectPath, SPPrefixType.WildcardInclusion);
            }
            return rootUri + "/" + projectPath + "/" + sharePointEventObject.ProjectSlug;
        }
    }
}

﻿using System.Diagnostics;

namespace Coscine.Api.LegacySharePoint.Utils
{
    class CommandUtil
    {
        public static void ExecuteCommand(string fileName, string arguments, string workingDirectory = null)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo
            {
                FileName = fileName,
                Arguments = arguments,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false,
                CreateNoWindow = true,
            };
            if (workingDirectory != null)
            {
                startInfo.WorkingDirectory = workingDirectory;
            }
            using (var process = new Process
            {
                StartInfo = startInfo
            })
            {
                process.Start();

                var output = process.StandardOutput.ReadToEnd();
                var error = process.StandardError.ReadToEnd();
            }
        }
    }
}

﻿using Coscine.Api.LegacySharePoint.Models;
using Coscine.Api.LegacySharePoint.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Coscine.Api.LegacySharePoint.Controllers
{
    public class SiteController : Controller
    {
        public const string Zone_TitleBar = @"TitleBar";
        public const string Zone_Header = @"Header";
        public const string Zone_Body = @"Body";
        public const string Zone_LeftColumn = @"LeftColumn";
        public const string Zone_MiddleColumn = @"MiddleColumn";
        public const string Zone_RightColumn = @"RightColumn";
        public const string Zone_Footer = @"Footer";

        public SiteController()
        {
        }

        [HttpPost("[controller]/onProjectCreate")]
        public IActionResult OnProjectCreate([FromBody] SharePointEventObject sharePointEventObject)
        {
            SPUserToken systemAccount = SPUserToken.SystemAccount;
            using (SPSite rootSite = new SPSite(sharePointEventObject.SharePointSite, systemAccount))
            {
                // XXX: get owner from configuration instead, this removes the need to instantiate the rootSite here!
                var projectUrl = SharePointUtil.EnsureProjectUrl(rootSite, sharePointEventObject);
                var owner = rootSite.RootWeb.SiteAdministrators[0];
                var ownerLogin = (owner.LoginName.StartsWith("i:") ? "" : "i:0#.w|") + owner.LoginName;
                rootSite.WebApplication.Sites.Add(new SPSiteCollectionAddParameters()
                {
                    SiteUrl = projectUrl,
                    Title = sharePointEventObject.ProjectDisplayName,
                    Description = sharePointEventObject.ProjectDescription,
                    LCID = (uint)1033,
                    WebTemplate = "STS#1",
                    OwnerLogin = ownerLogin,
                    OwnerName = owner.Name,
                    OwnerEmail = owner.Email,
                    CreateFromSiteMaster = true
                });

                using (SPSite projectSite = new SPSite(projectUrl, systemAccount))
                {
                    // Make it bilingual (first because otherwise the update function might be unsync)
                    projectSite.RootWeb.IsMultilingual = true;
                    SPLanguageCollection installedLanguages = projectSite.RootWeb.RegionalSettings.InstalledLanguages;
                    IEnumerable<CultureInfo> supportedLanguages = projectSite.RootWeb.SupportedUICultures;
                    foreach (SPLanguage language in installedLanguages)
                    {
                        CultureInfo culture = new CultureInfo(language.LCID);
                        if (!supportedLanguages.Contains(culture))
                        {
                            projectSite.RootWeb.AddSupportedUICulture(culture);
                        }
                    }
                    projectSite.RootWeb.EnableMinimalDownload = false;
                    projectSite.RootWeb.Update();

                    // Activate Template and VueWebPart Features
                    var templateFeatureId = Guid.Parse("6cb5c1d2-c486-4af7-bcd7-ac6d20bee59f");
                    var vueWebPartFeatureId = Guid.Parse("f3580f14-de70-4b63-b22d-d2d29eb36392");
                    var modernExperienceFeatureId = Guid.Parse("E3540C7D-6BEA-403C-A224-1A12EAFEE4C4");

                    if (SPFarm.Local.FeatureDefinitions[templateFeatureId] != null)
                    {
                        projectSite.RootWeb.Features.Add(templateFeatureId);
                    }

                    if (SPFarm.Local.FeatureDefinitions[vueWebPartFeatureId] != null)
                    {
                        projectSite.Features.Add(vueWebPartFeatureId);
                    }

                    if (SPFarm.Local.FeatureDefinitions[modernExperienceFeatureId] != null)
                    {
                        projectSite.Features.Add(modernExperienceFeatureId);
                    }

                    // Add Groups
                    AddSPGroup("Owner", projectSite.RootWeb);
                    AddSPGroup("Member", projectSite.RootWeb);

                    // Remove Features
                    var removeFeatures = new Dictionary<string, SPFeatureCollection> {
                        // Mobile view
                        { "d95c97f3-e528-4da2-ae9f-32b3535fbb59", projectSite.RootWeb.Features },
                        // FollowingContent
                        { "a7a2793e-67cd-4dc1-9fd0-43f61581207a", projectSite.RootWeb.Features }
                    };

                    foreach (var removeFeature in removeFeatures)
                    {
                        var removeFeatureId = Guid.Parse(removeFeature.Key);
                        if (SPFarm.Local.FeatureDefinitions[removeFeatureId] != null)
                        {
                            var feature = removeFeature.Value[removeFeatureId];
                            if (feature != null)
                            {
                                removeFeature.Value.Remove(removeFeatureId);
                            }
                        }
                    }
                }
            }

            using (var pageUtil = new PageUtil())
            {
                pageUtil.DeployProjectPages(sharePointEventObject);
            }
            return Ok();
        }

        [HttpPost("[controller]/onProjectDelete")]
        public IActionResult OnProjectDelete([FromBody] SharePointEventObject sharePointEventObject)
        {
            SPUserToken systemAccount = SPUserToken.SystemAccount;
            using (SPSite site = new SPSite(sharePointEventObject.SharePointSite, systemAccount))
            {
                var projectUrl = SharePointUtil.EnsureProjectUrl(site, sharePointEventObject);
                using (SPSite projectSite = new SPSite(projectUrl, systemAccount))
                {
                    projectSite.Delete();
                }
            }
            return Ok();
        }

        private void AddSPGroup(string name, SPWeb web)
        {
            web.SiteGroups.Add(name, web.SiteAdministrators[0], null, "");

            SPGroup spGroup = web.SiteGroups[name];

            SPRoleDefinition spRole = web.RoleDefinitions["Contribute"];
            SPRoleAssignment roleAssignment = new SPRoleAssignment(spGroup);
            roleAssignment.RoleDefinitionBindings.Add(spRole);

            web.RoleAssignments.Add(roleAssignment);
        }
    }
}

﻿using Coscine.Api.LegacySharePoint.Models;
using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Coscine.Api.LegacySharePoint.Utils
{
    public class PageUtil : IDisposable
    {
        private readonly string _tempDirectory;

        public PageUtil()
        {
            var tempGuid = Guid.NewGuid().ToString();
            _tempDirectory = Path.Combine("C:\\coscine", tempGuid);

            // Using a ps1 script since C# git implementations are lacking
            CommandUtil.ExecuteCommand(
                "powershell.exe",
                PowerShellUtil.GetTemplatesScript(_tempDirectory)
            );
        }

        public void DeployProjectPages(SharePointEventObject sharePointEventObject)
        {
            SPUserToken systemAccount = SPUserToken.SystemAccount;
            using (SPSite rootSite = new SPSite(sharePointEventObject.SharePointSite, systemAccount))
            {
                var projectUrl = SharePointUtil.EnsureProjectUrl(rootSite, sharePointEventObject);
                using (SPSite projectSite = new SPSite(projectUrl, systemAccount))
                {
                    DeployPages(projectSite, Path.Combine(_tempDirectory, "Project"));
                }
            }
        }

        public void DeployRootPages(SharePointEventObject sharePointEventObject)
        {
            SPUserToken systemAccount = SPUserToken.SystemAccount;
            using (SPSite rootSite = new SPSite(sharePointEventObject.SharePointSite, systemAccount))
            {
                DeployPages(rootSite, Path.Combine(_tempDirectory, "Root"));
            }
        }

        private void DeployPages(SPSite site, string path)
        {
            SPFolder libraryFolder = site.RootWeb.GetFolder("SitePages");
            if (!libraryFolder.Exists)
            {
                site.RootWeb.Lists.Add("SitePages", "SitePages", SPListTemplateType.DocumentLibrary);
                libraryFolder = site.RootWeb.GetFolder("SitePages");
            }

            SPFileCollection files = libraryFolder.Files;

            DeleteAllFiles(files);

            foreach (var file in Directory.EnumerateFiles(path))
            {
                var fileInfo = new FileInfo(file);
                string data = File.ReadAllText(file);
                data = EmbedSPWebPart(site, data, "[[DiscussionBoardId]]", "Discussion Board", SPListTemplateType.DiscussionBoard);
                data = EmbedSPWebPart(site, data, "[[AnnouncementBoardId]]", "Announcement Board", SPListTemplateType.Announcements);
                data = EmbedSPWebPart(site, data, "[[DocumentLibraryId]]", "Shared Documents", SPListTemplateType.DocumentLibrary);
                byte[] bytes = Encoding.ASCII.GetBytes(data);
                using (var stream = new MemoryStream(bytes))
                {
                    files.Add(fileInfo.Name, stream);
                }
            }

            SPWeb web = site.RootWeb;
            if (web != null)
            {
                SPFolder rootFolder = web.RootFolder;
                rootFolder.WelcomePage = "SitePages/Home.aspx";
                rootFolder.Update();
            }

        }

        private string EmbedSPWebPart(SPSite site, string data, string key, string identifier, SPListTemplateType templateType)
        {
            if (data.Contains(key))
            {
                if (site.RootWeb.Lists.TryGetList(identifier) == null)
                {
                    site.RootWeb.Lists.Add(identifier, identifier, templateType);
                }
                var spList = site.RootWeb.Lists[identifier];
                return data.Replace(key, spList.ID.ToString("B").ToUpper());
            }
            return data;
        }

        private void DeleteAllFiles(SPFileCollection files)
        {
            // Creating a list so that "files" does not get modified and throws an Exception
            var fileList = new List<SPFile>();
            foreach (SPFile file in files)
            {
                fileList.Add(file);
            }
            foreach (var file in fileList)
            {
                file.Delete();
            }
        }
        
        public void Dispose()
        {
            // Using the command since Directory.Delete won't delete everything in .git
            CommandUtil.ExecuteCommand("cmd.exe", "/c " + $@"rmdir /s/q {_tempDirectory}");
        }
    }
}
